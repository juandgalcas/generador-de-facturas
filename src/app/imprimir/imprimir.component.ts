import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConexionService } from '../servicios/conexion.service';
 
@Component({
  selector: 'app-imprimir',
  templateUrl: './imprimir.component.html',
  styleUrls: ['./imprimir.component.css']
})
export class ImprimirComponent implements OnInit {

  factura: any = {'idn': '', 'nombre': '', 'apellido': '', 'fecha': '', 'descripcion': [], 'total': 0, 'cantidades': []};
  descripciones: any[] = [];
  id: string;
  arregloDiligencias: any[] = [];
  arregloDescripciones: any = [];
  diligencias: any[];
  condicional: boolean = true;

  constructor(private conexionService: ConexionService, private router: Router, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.conexionService.getFactura(this.id).subscribe(
      (factura: any) => {
        this.factura = factura;
      })
 
    this.conexionService.getDescripcion().subscribe(
      descripciones => {
        this.descripciones = descripciones;
      })

    this.condicional = true;
  }

  descripcionFactura(factura: any){
    this.arregloDiligencias = factura.cantidades;
    this.arregloDescripciones = this.descripciones;
    for (let descripcion of this.descripciones){
      for (let diligencia of this.factura.cantidades){
        if (descripcion.diligencia == diligencia.diligencia){
          let indice = this.arregloDescripciones.indexOf(descripcion);
          this.arregloDescripciones.splice(indice, 1);
        }
      }
    }
    this.condicional = false;
  }
}