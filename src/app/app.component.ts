import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  today: Date = new Date();
  pipe = new DatePipe('en-US');
  fechaFactura: any = '';
  ngOnInit(): void {
    this.fechaFactura = this.pipe.transform(Date.now(), 'dd/MM/yyyy');
  }
  
}
