// import { httpclient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/compat/firestore';
import { Factura } from '../modelo/factura.model';
import { Descripcion } from '../modelo/descripcion.model';

@Injectable()
export class ConexionService{
    facturaColeccion: AngularFirestoreCollection<Factura>;
    facturaDoc: AngularFirestoreDocument<Factura>;
    facturas: Observable<Factura[]>;
    factura: Observable<any>;
    facturaEspecifica: Observable<any>;

    descripcionColeccion: AngularFirestoreCollection<Descripcion>;
    descripcionDoc: AngularFirestoreDocument<Descripcion>;
    descripciones: Observable<Descripcion[]>;
    descripcion: Observable<Descripcion>;

    facturaImpresion: Factura = {'idn': '', 'fecha': '', 'nombre': '', 'apellido': '', 'descripcion': [], 'total': 0}; 
    diligenciaAgregada: any[] = [];
    inicio: boolean = false;

    constructor(private db: AngularFirestore){
        this.facturaColeccion = db.collection('Factura', ref => ref.orderBy('nombre', 'asc'));
        this.descripcionColeccion = db.collection('Descripcion', ref => ref.orderBy('diligencia', 'asc'));
    }

    getFacturas(): Observable<Factura[]>{
        this.facturas = this.facturaColeccion.snapshotChanges().pipe(
            map( (cambios: any) => {
                return cambios.map(
                    (accion: any) => {
                        const datos = accion.payload.doc.data() as Factura;
                        return datos;
                    }
                )
            })
        );
        return this.facturas;
    }
    getContenido(id: any){
        this.facturaEspecifica = this.db.collection('Factura').snapshotChanges().pipe(
            map((contenido: any) => contenido.map((estado: any) =>{
                let data = estado.payload.doc.data();
                let idFacturaSeleccionada = estado.payload.doc.id;
                if (id == data.idn){
                    return idFacturaSeleccionada;
                }
            })));
        return this.facturaEspecifica;
    }
    getFactura(id: string){
        this.facturaDoc = this.db.doc<Factura>(`Factura/${id}`);
        this.factura = this.facturaDoc.snapshotChanges().pipe(
            map((accion: any) => {
                if(accion.payload.exists === false){
                    return null;
                }else {
                    let datos = accion.payload.data() as Factura;
                    return datos;
                }
            })
        );
        return this.factura;
    }
    getDescripcion(): Observable<Descripcion[]>{
        this.descripciones = this.descripcionColeccion.snapshotChanges().pipe(
            map( (cambio: any) => {
                return cambio.map(
                    (acciones: any) => {
                        const dato = acciones.payload.doc.data() as Descripcion;
                        dato.id = acciones.payload.doc.id;
                        return dato;
                    }
                )
            })
        );
        return this.descripciones;
    }

    añadirDescripcion(descripcion: Descripcion){
        this.descripcionColeccion.add(descripcion);
    }
    añadirFactura(factura: Factura){
        this.facturaColeccion.add(factura);
    }
    modificarDescripcion(descripcion: Descripcion){
        this.descripcionDoc = this.db.doc(`Descripcion/${descripcion.id}`);
        this.descripcionDoc.update(descripcion);
    }
    modificarFactura(factura: Factura){
        this.facturaDoc = this.db.doc(`Factura/${factura.idn}`);
        this.facturaDoc.update(factura);
    }
    eliminarFactura(factura: Factura){
        this.facturaDoc = this.db.doc(`Factura/${factura.idn}`);
        this.facturaDoc.delete();
    }
    eliminarDescripcion(descripcion: Descripcion){
        this.descripcionDoc = this.db.doc(`Descripcion/${descripcion.id}`);
        this.descripcionDoc.delete();
    }
    imprimir(factura: any, diligenciaAgregada: any){
        this.facturaImpresion = factura;
        this.diligenciaAgregada = diligenciaAgregada;
    }
}