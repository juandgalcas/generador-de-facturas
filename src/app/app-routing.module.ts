import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearFacturaComponent } from './crear-factura/crear-factura.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { ErrorComponent } from './error/error.component';
import { GenerarFacturaComponent } from './generar-factura/generar-factura.component';
import { ImprimirComponent } from './imprimir/imprimir.component';
import { ModificarFacturaComponent } from './modificar-factura/modificar-factura.component';

const routes: Routes = [
  {path: '', component: EncabezadoComponent},
  {path: 'crearFactura', component: CrearFacturaComponent},
  {path: 'imprimir', component: ImprimirComponent},
  {path: 'generarFactura', component: GenerarFacturaComponent, children: [
    {path: 'modificar/:id', component: ModificarFacturaComponent},
    {path: 'imprimir/:id', component: ImprimirComponent}
  ]},
  {path: '**', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
