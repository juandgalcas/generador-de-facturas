import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Descripcion } from '../modelo/descripcion.model';
import { Factura } from '../modelo/factura.model';
import { ConexionService } from '../servicios/conexion.service';

@Component({
  selector: 'app-modificar-factura',
  templateUrl: './modificar-factura.component.html',
  styleUrls: ['./modificar-factura.component.css']
})
export class ModificarFacturaComponent implements OnInit {

  factura: any = {'idn': '', 'nombre': '', 'apellido': '', 'fecha': '', 'descripcion': [], 'total': 0, 'cantidades': []};
  descripciones: any[] = [];
  id: string;
  fecha: string;
  nombre: string;
  apellido: string;
  nuevaDiligencia: any;
  arregloDiligencias: any[] = [];
  diligencias: any[];
  condicional: boolean = true;
  DiligenciaNombre: any;
  DiligenciaPrecio: any;
  DiligenciaCantidad: any;
  precioDiligencia: any;
  cantidadDiligencia: any;
  nodoCantidad: boolean;
  cantidadDiligenciaAnterior: any;
  arregloDescripcion: any[] = [];

  constructor(private conexionService: ConexionService, private router: Router, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.conexionService.getFactura(this.id).subscribe(
      (factura: any) => {
        this.factura = factura;
      })
 
    this.conexionService.getDescripcion().subscribe(
      descripciones => {
        this.descripciones = descripciones;
        this.arregloDescripcion = descripciones;
      })

    this.condicional = true;
    console.log(this.factura);
  }

  descripcionFactura(factura: any){
    let contador = 0;
    this.arregloDiligencias = factura.cantidades;
    let descripcion: any[] = [];
    let indices: any[] =[];
    for (let des of this.descripciones){
      descripcion.push(des.diligencia);
    }
    for (let diligencia of factura.cantidades){
      if (descripcion.indexOf(diligencia.diligencia) == -1){
        descripcion.push(diligencia.diligencia);
        this.descripciones.push(diligencia);
        this.arregloDescripcion.push(diligencia);
      }
      let indice = descripcion.indexOf(diligencia.diligencia);
      indices.push(indice);
      this.descripciones[indice].cantidad = diligencia.cantidad;
      this.descripciones[indice].precio = diligencia.precio;
      this.descripciones[indice].click = true;
      if (contador = 0){
        this.arregloDescripcion[indice].cantidad = diligencia.cantidad;
        this.arregloDescripcion[indice].precio = diligencia.precio;
        this.arregloDescripcion[indice].click = diligencia.click;
        contador += 1;
      }
    }
    this.condicional = false;
  }

  click(descripcion: any, indice: any){
    if (!descripcion.click){
      if (this.descripciones[indice].diligencia == descripcion.diligencia){
        this.descripciones[indice].click = true;
        this.arregloDescripcion[indice].click = true;
      }
    }
    else if(descripcion.click){
      if (this.descripciones[indice].diligencia == descripcion.diligencia){
        this.descripciones[indice].click = false;
        this.arregloDescripcion[indice].click = false;
      }
    } 
  }

  agregarDiligencia(){
    if (this.DiligenciaCantidad == 0){
      this.DiligenciaCantidad = 1;
      alert('La cantidad no puede ser 0, se define automáticamente en 1');
    }
    if (((this.DiligenciaNombre != null) && (this.DiligenciaNombre != ' ')) && (this.DiligenciaPrecio != 0)){
      let descripcion: any[] = [];
      for (let des of this.descripciones){
        descripcion.push(des.diligencia);
      }
      if (descripcion.indexOf(this.DiligenciaNombre.toUpperCase()) >= 0){
        alert('Diligencia repetida');
      } 
      if (descripcion.indexOf(this.DiligenciaNombre.toUpperCase()) == -1){
        let descripcion: Descripcion = {'diligencia': this.DiligenciaNombre.toUpperCase(), 'id': `${this.descripciones?.length + 1}`, 'precio': this.DiligenciaPrecio, 'click': true, 'cantidad': this.DiligenciaCantidad};
        this.descripciones.push(descripcion);
      }      
    }
    (this.DiligenciaNombre == null) && alert('Nombre diligencia vacia');
    (this.DiligenciaNombre == ' ') && alert('Nombre diligencia incorrecta (espacio vacio)');
    (this.DiligenciaPrecio == 0) && alert('Precio diligencia nula');
    this.DiligenciaNombre = '';
    this.DiligenciaPrecio = 0;
    this.DiligenciaCantidad = 0;
    this.nuevaDiligencia = false;
  }
  guardarPrecio(valor: any){
    this.precioDiligencia =  valor.target.value;
  }

  guardarCantidad(valor: any){
    this.cantidadDiligencia = valor.target.value;
    if (+this.cantidadDiligencia == 0){
      alert('La cantidad no puede ser 0, automáticamente será 1');
      this.cantidadDiligencia = 1;
    }
    this.nodoCantidad = true;
  }

  nuevoPrecioCantidad(valor: any){
    if (this.precioDiligencia == ' '){
      alert('Si no se agrega un valor se utilizará el almacenado anteriorimente');
      for (let descripcion of this.descripciones){
        if (valor.diligencia == descripcion.diligencia){
          this.precioDiligencia = valor.precio;
        }
      }
    }
    if (this.cantidadDiligencia == 0){
      this.cantidadDiligencia = 1;
    }
    else{
      (!this.nodoCantidad) && alert('Si no se indica un valor, automáticamente la cantidad será 1');
      this.cantidadDiligenciaAnterior = valor.cantidad;
    }
    (this.precioDiligencia == ' ') && alert('Precio vacio');
    (this.precioDiligencia == '0') && alert('No puede ser "0"');
    if ((this.precioDiligencia != '0') && (this.precioDiligencia != ' ')){
      for (let descripcion of this.descripciones){
        if (valor.click){
          if (valor == descripcion){
            let nuevaDescripcion = {'id': valor.id, 'diligencia': valor.diligencia, 'precio': +this.precioDiligencia, 'click': valor.click, 'cantidad': +this.cantidadDiligencia};
            this.conexionService.modificarDescripcion(nuevaDescripcion);
          }
        }
      }
      this.precioDiligencia = 0;
      this.cantidadDiligencia = 1;
      this.nodoCantidad = false;
    }
  }

  modificarFactura(){

  }
}