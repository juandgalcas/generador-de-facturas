import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Descripcion } from '../modelo/descripcion.model';
import { Factura } from '../modelo/factura.model';
import { ConexionService } from '../servicios/conexion.service';

@Component({
  selector: 'app-generar-factura',
  templateUrl: './generar-factura.component.html',
  styleUrls: ['./generar-factura.component.css']
})
export class GenerarFacturaComponent implements OnInit {

  facturas: Factura[];
  descripciones: Descripcion[];
  idFactura: any;
  idImpresion: any;
  route: boolean = false;

  constructor(private conexionService: ConexionService, private router: Router) { }

  ngOnInit(): void {
    this.conexionService.getFacturas().subscribe(
      facturas => {
        this.facturas = facturas;
      }
    )
  this.route = false;
  }

  imprimir(id: any){
    this.conexionService.getContenido(id).subscribe(
      (contenido: any) => {
        let cont = contenido;
        for (let ids of cont){
          if (ids != null){
            this.idImpresion = ids;
            this.router.navigate([`/generarFactura/imprimir/` + this.idImpresion]);
          }
        }
      }
    )
    this.route = true;
  }
  modificar(id: any){
    this.conexionService.getContenido(id).subscribe(
      (contenido: any) => {
        let cont = contenido;
        for (let ids of cont){
          if (ids != null){
            this.idImpresion = ids;
            this.router.navigate([`/generarFactura/modificar/`, this.idImpresion]);
          }
        }
      }
    )
    this.route = true;
  }
  eliminarFactura(factura: Factura){
    if(confirm('¿Seguro que desea eliminar el cliente?')){
      this.conexionService.eliminarFactura(factura);
      this.router.navigate(['/']);
    }
  }
  buscarFactura(valor: any){
    
  }
}