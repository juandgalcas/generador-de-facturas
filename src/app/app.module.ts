import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule, SETTINGS } from '@angular/fire/compat/firestore';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrearFacturaComponent } from './crear-factura/crear-factura.component';
import { GenerarFacturaComponent } from './generar-factura/generar-factura.component';
import { ErrorComponent } from './error/error.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { ConexionService } from './servicios/conexion.service';
import { ModificarFacturaComponent } from './modificar-factura/modificar-factura.component';
import { ImprimirComponent } from './imprimir/imprimir.component';

@NgModule({
  declarations: [
    AppComponent,
    CrearFacturaComponent,
    GenerarFacturaComponent,
    ErrorComponent,
    EncabezadoComponent,
    ModificarFacturaComponent,
    ImprimirComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firestore, 'Factura'),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [ConexionService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
