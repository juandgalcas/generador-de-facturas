import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Descripcion } from '../modelo/descripcion.model';
import { Factura } from '../modelo/factura.model';
import { ConexionService } from '../servicios/conexion.service';

@Component({
  selector: 'app-crear-factura',
  templateUrl: './crear-factura.component.html',
  styleUrls: ['./crear-factura.component.css']
})
export class CrearFacturaComponent implements OnInit {
  
  // fecha automática
  today: Date = new Date();
  pipe = new DatePipe('en-US');
  fechaFormulario: any = '';

  // valores formulario
  fechaEntrada = '';
  nombreFormulario: string;
  apellidoFormulario: string;
  DiligenciaNombre: string;
  DiligenciaPrecio: number = 0; 
  DiligenciaCantidad: number = 0;
  idFactura: string;
  descripcionEntrada: string;
  diligenciaCheck: string;
  descripcionCheckbox: any[] = [];
  precioDiligencia: any = 0;
  cantidadDiligencia: any = 1;
  cantidadDiligenciaAnterior: any = 1;
  totalFactura: any = 0;
  arregloCantidades: any[] = [];

  // Variables formulario
  automatico: boolean = false;
  otro: boolean = false;
  arregloDiligencias: any[] = [];
  factura: Factura = {'idn': '', 'nombre': '', 'apellido': '', 'fecha': '', 'descripcion': [], 'total': 0, 'cantidades': []};
  totalFormulario: any = 0;
  chequeados: any[] = [];
  noChequeados: any[] = [];
  descripcionAgregada: any[] = [];
  pinImprimir: boolean = false;
  cantidadPrecio: any = 0;

  descripcionDiligencia: Descripcion[];
  descripciones: Descripcion[];
  facturas: Factura[];
  descripcionFactura: any;
  validacionDescripcion: boolean = false;
  nodoCantidad: boolean = false;
  nodoCancelar: boolean = false;
  
  constructor(private conexionService: ConexionService, private router: Router, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.fechaFormulario = this.pipe.transform(Date.now(), 'dd/MM/yyyy');
    
    this.conexionService.getDescripcion().subscribe(
      descripciones => {
        this.descripciones = descripciones;
      }
    )
    
    this.conexionService.getFacturas().subscribe(
      facturas => {
        this.facturas = facturas;
      }
    )
  }

  fechaAutomatica(){
    this.fechaEntrada = this.fechaFormulario;
    this.automatico = true;
  }

  fechaManual(){
    this.fechaEntrada = '';
    this.automatico = false;
  }

  guardarPrecio(valor: any){
    this.precioDiligencia =  valor.target.value;
  }

  guardarCantidad(valor: any){
    this.cantidadDiligencia = valor.target.value;
    if (+this.cantidadDiligencia == 0){
      alert('La cantidad no puede ser 0, automáticamente será 1');
      this.cantidadDiligencia = 1;
    }
    this.nodoCantidad = true;
  }

  nuevoPrecioCantidad(valor: any){
    if (this.precioDiligencia == ' '){
      alert('Si no se agrega un valor se utilizará el almacenado anteriorimente');
      for (let descripcion of this.descripciones){
        if (valor.diligencia == descripcion.diligencia){
          this.precioDiligencia = valor.precio;
        }
      }
    }
    if (this.cantidadDiligencia == 0){
      this.cantidadDiligencia = 1;
    }
    else{
      (!this.nodoCantidad) && alert('Si no se indica un valor, automáticamente la cantidad será 1');
      this.cantidadDiligenciaAnterior = valor.cantidad;
    }
    (this.precioDiligencia == ' ') && alert('Precio vacio');
    (this.precioDiligencia == '0') && alert('No puede ser "0"');
    if ((this.precioDiligencia != '0') && (this.precioDiligencia != ' ')){
      for (let descripcion of this.descripciones){
        if (valor.click){
          if (valor == descripcion){
            this.noChequeados.push({'precio':+valor.precio, 'cantidad': +this.cantidadDiligenciaAnterior});
            this.chequeados.push({'precio':+this.precioDiligencia, 'cantidad': +this.cantidadDiligencia});
            let nuevaDescripcion = {'id': valor.id, 'diligencia': valor.diligencia, 'precio': +this.precioDiligencia, 'click': valor.click, 'cantidad': +this.cantidadDiligencia};
            this.conexionService.modificarDescripcion(nuevaDescripcion);
          }
        }
      }
      this.calcularTotal();
      this.precioDiligencia = 0;
      this.cantidadDiligencia = 1;
      this.nodoCantidad = false;
    }
  }

  click(descripcion: any){
    for (let des of this.descripciones){
      if (!descripcion.click){
        if (des == descripcion){
          this.chequeados.push({'precio':des.precio, 'cantidad': descripcion.cantidad});
          des.click = true;
          this.conexionService.modificarDescripcion(des); 
          this.descripcionCheckbox.push(descripcion.diligencia);
          break;
        }
      }
      else if(descripcion.click){
        if (des == descripcion){
          let index = this.descripcionCheckbox.indexOf(des.diligencia);  
          this.noChequeados.push({'precio':des.precio, 'cantidad': descripcion.cantidad});
          des.click = false;
          this.conexionService.modificarDescripcion(des);  
          this.descripcionCheckbox.splice(index, 1);
          break;
        }
      } 
    }
    this.calcularTotal();
  }

  agregarDiligencia(){
    if (this.DiligenciaCantidad == 0){
      this.DiligenciaCantidad = 1;
      alert('La cantidad no puede ser 0, se define automáticamente en 1');
    }
    if (((this.DiligenciaNombre != null) && (this.DiligenciaNombre != ' ')) && (this.DiligenciaPrecio != 0)){
      let descripcion: any[] = [];
      for (let des of this.descripciones){
        descripcion.push(des.diligencia);
      }
      if (descripcion.indexOf(this.DiligenciaNombre.toUpperCase()) >= 0){
        alert('Diligencia repetida');
      } 
      if (descripcion.indexOf(this.DiligenciaNombre.toUpperCase()) == -1){
        let descripcion: Descripcion = {'diligencia': this.DiligenciaNombre.toUpperCase(), 'id': `${this.descripciones?.length + 1}`, 'precio': this.DiligenciaPrecio, 'click': true, 'cantidad': this.DiligenciaCantidad};
        this.conexionService.añadirDescripcion(descripcion);
        this.chequeados.push({'precio':descripcion.precio, 'cantidad': this.DiligenciaCantidad});
        this.descripcionAgregada.push(descripcion.diligencia);
        this.calcularTotal();
        this.descripcionCheckbox.push(descripcion.diligencia);
      }      
    }
    (this.DiligenciaNombre == null) && alert('Nombre diligencia vacia');
    (this.DiligenciaNombre == ' ') && alert('Nombre diligencia incorrecta (espacio vacio)');
    (this.DiligenciaPrecio == 0) && alert('Precio diligencia nula');
    this.DiligenciaNombre = '';
    this.DiligenciaPrecio = 0;
    this.DiligenciaCantidad = 0;
    this.otro = false;
  }

  sumar(valores: any){
    let suma = 0;
    for (let valor of valores){
      suma += (valor.precio * valor.cantidad);
    }
    return suma;
  }

  restar(valores: any){
    let resta = 0;
    for (let valor of valores){
      resta += (valor.precio * valor.cantidad);
    }
    return resta;
  }

  calcularTotal(){
    this.totalFactura = this.sumar(this.chequeados) - this.restar(this.noChequeados);
  }

  eliminarDescripcion(descripcion: Descripcion){
    this.conexionService.eliminarDescripcion(descripcion);
  }
  
  enviarFormulario(){
    // Generar identificador de la factura
    for (let i = 0; i < this.facturas?.length; i++){
      if (i <= 9){
        this.idFactura = '000' + (i+1).toString();
      }
      else if (i > 9 && i < 100){
        this.idFactura = '00' + (i+1).toString();
      }
      else if (i > 99 && i < 1000){
        this.idFactura = '0' + (i+1).toString();
      }
      else {
        this.idFactura = (i+1).toString();
      }
    }
    
    let nuevaFactura: Factura = {'idn': this.idFactura, 'fecha': this.fechaEntrada, 'nombre': this.nombreFormulario, 'apellido': this.apellidoFormulario, 'descripcion': this.descripcionCheckbox, 'total': this.totalFactura, 'cantidades': this.arregloCantidades};
    (this.fechaEntrada == '') && alert('Ingresar Fecha');
    ((this.nombreFormulario == null) || (this.nombreFormulario == '')) && alert('Ingresar nombre');
    ((this.apellidoFormulario == null) || (this.apellidoFormulario == '')) && alert('Ingresar apellido');
    (this.totalFactura == 0) && alert('Seleccionar por lo menos una diligencia');

    for (let des of this.descripciones){ 
      for (let check of this.descripcionCheckbox){
        if (check == des.diligencia){
          this.arregloCantidades.push({'diligencia': des.diligencia, 'cantidad': des.cantidad, 'precio': des.precio});
          break;
        }
      }
      des.click = false;
      des.cantidad = 1;
      this.conexionService.modificarDescripcion(des); 
    } 

    if (!((this.fechaEntrada != '')&&(this.nombreFormulario != null)&&(this.nombreFormulario != ' ')&&(this.apellidoFormulario != null)&&(this.apellidoFormulario != ' ')&&(this.totalFactura != 0))){
      for (let des of this.descripciones){
        for (let desc of this.descripcionAgregada){
          if (desc == des.diligencia){
            this.conexionService.eliminarDescripcion(des);
          }
        }
      }
    } else{
      this.conexionService.añadirFactura(nuevaFactura);
      this.conexionService.imprimir(nuevaFactura, this.descripcionAgregada);
      this.pinImprimir = true;
    }

    this.automatico = false;
    this.fechaEntrada = '';
    this.nombreFormulario = '';
    this.apellidoFormulario = '';
    this.descripcionCheckbox = [];
    this.chequeados = [];
    this.totalFactura = 0;
    this.arregloCantidades = [];
    this.pinImprimir = true;
  }

  cancelar(){
    for (let des of this.descripciones){ 
      des.click = false;
      des.cantidad = 1;
      this.conexionService.modificarDescripcion(des); 
      for (let desc of this.descripcionAgregada){
        if (desc == des.diligencia){
          this.conexionService.eliminarDescripcion(des);
        }
      }
    } 
    
    this.fechaEntrada = '';
    this.nombreFormulario = '';
    this.apellidoFormulario = '';
    this.descripcionCheckbox = [];
    this.chequeados = [];
    this.noChequeados = [];
    this.totalFactura = 0;
    this.nodoCancelar = true;
  }

  nuevaFactura(){
    this.pinImprimir = false;
    this.nodoCancelar = false;
  }
}