export interface Descripcion{
    id?: string;
    diligencia?: string;
    precio?: any;
    click?: boolean;
    cantidad?: number;
}