import { Descripcion } from "./descripcion.model";

export interface Factura{
    idn?: string;
    fecha?: string;
    nombre?: string;
    apellido?: string;
    descripcion?: Descripcion[];
    total?: number;
    cantidades?: number[];
}